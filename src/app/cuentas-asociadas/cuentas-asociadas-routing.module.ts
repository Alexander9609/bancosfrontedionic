import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentasAsociadasPage } from './cuentas-asociadas.page';

const routes: Routes = [
  {
    path: '',
    component: CuentasAsociadasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentasAsociadasPageRoutingModule {}
