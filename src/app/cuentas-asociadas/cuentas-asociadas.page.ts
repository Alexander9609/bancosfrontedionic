import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { CuentaService } from '../services/entities/cuenta/cuenta.service';
import { CuentasAsociadasService } from '../services/entities/cuentasAsociadas/cuentas-asociadas.service';
import { CuentasAsociadas, ICuentasAsociadas } from '../shared/model/cuentas-asociadas.model';

@Component({
  selector: 'app-cuentas-asociadas',
  templateUrl: './cuentas-asociadas.page.html',
  styleUrls: ['./cuentas-asociadas.page.scss'],
})
export class CuentasAsociadasPage implements OnInit {
  status: boolean
  cuentaEncontrada;
  found: boolean = false
  account;
  cuentas
  segmentTab: string = "cuentas";

  constructor(    public toastController: ToastController,
    private fb: FormBuilder, private cuentaService: CuentaService, private cuentasAsociadasService: CuentasAsociadasService
  ) { }
  editForm = this.fb.group({
    numerCuenta: [],

  });
  editForm2 = this.fb.group({
    aliasCuentaAsocia: [],

  });
  ngOnInit() {
    this.account = JSON.parse(localStorage.getItem('dataAccount'))
    this.status = true
    this.getCuentas()
  }
  search() {
    console.log();
    this.cuentaService.findCuentasByCuenta(this.editForm.get('numerCuenta').value).subscribe(succes => {
      this.found = true
      this.cuentaEncontrada = succes.body[0]
      console.log(this.cuentaEncontrada);
      if (this.cuentaEncontrada === undefined) {
        this.found = false
      }
    }, error => {
      this.found = false
    })

  }

  async saveCuentaAsociada() {
    const cuentasAsociadas = this.createFromForm();
    this.subscribeToSaveResponse(this.cuentasAsociadasService.create(cuentasAsociadas));
    const toast = await this.toastController.create({
      
      message: "Cuenta vinculada ",
      duration: 5000,
      position: 'middle'
    });
    this.found = false
    toast.present();
  }
  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICuentasAsociadas>>) {
    result.subscribe((res: HttpResponse<ICuentasAsociadas>) => { }, (res: HttpErrorResponse) => { })
  }
  private createFromForm(): ICuentasAsociadas {
    const entity = {
      ...new CuentasAsociadas(),
      aliasCuentaAsocia: this.editForm2.get('aliasCuentaAsocia').value,
      user: this.account,
      cuenta: this.cuentaEncontrada
    };
    return entity;
  }

  segmentChanged(event: any) {
    this.segmentTab = event.detail.value;
  }

  //cuentas get

  getCuentas(){
    this.cuentasAsociadasService.findCuentasByUserId(this.account.id).subscribe(succesCuentas=>{
      this.cuentas=succesCuentas.body
      console.log("adwaihdai",succesCuentas.body);
      
    })
  }

}
