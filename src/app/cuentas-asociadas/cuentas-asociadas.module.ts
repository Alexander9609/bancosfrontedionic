import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentasAsociadasPageRoutingModule } from './cuentas-asociadas-routing.module';

import { CuentasAsociadasPage } from './cuentas-asociadas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CuentasAsociadasPageRoutingModule
  ],
  declarations: [CuentasAsociadasPage]
})
export class CuentasAsociadasPageModule {}
