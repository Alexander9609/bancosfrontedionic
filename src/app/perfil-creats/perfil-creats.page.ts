import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from '../shared/constants/input.constants';
import {  IPerfil, Perfil } from '../shared/model/perfil.model';
import { PerfilService } from '.././services/entities/perfil/perfil.service';
import { User, } from '../services/user/user.model';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-perfil-creats',
  templateUrl: './perfil-creats.page.html',
  styleUrls: ['./perfil-creats.page.scss'],
})
export class PerfilCreatsPage implements OnInit {
  perfil: IPerfil;
  isSaving: boolean;

  users: User[];
  usuario
  editForm = this.formBuilder.group({
    id: [],
    tipoIdentificacion: [],
    identificacion: [],
    edad: [],
    sexo: [],
    direccion: [],
    fechaNacimiento: [],
    nacionalidad: [],
    telefono: [],
    user: []
  });


  constructor(
    protected perfilService: PerfilService,
    protected userService: UserService,
    protected formBuilder: FormBuilder,
    protected activatedRoute: ActivatedRoute,
  ) {

  }



  ngOnInit() {
    this.isSaving = false;
    let account = JSON.parse(localStorage.getItem('dataAccount'))
    this.perfilService.findByUser(account.id).subscribe(perfil => {
      
      if (perfil.body['length'] !== 0) {
        this.updateForm(perfil.body[0]);
        this.perfil = perfil.body[0];
        this.isSaving=true
      }

    }, error => {
      console.log("##########", error);

    })

    this.usuario = JSON.parse(localStorage.getItem('dataAccount'))

  }

  updateForm(perfil: Perfil) {
    this.editForm.patchValue({
      id: perfil.id,
      tipoIdentificacion: perfil.tipoIdentificacion,
      identificacion: perfil.identificacion,
      edad: perfil.edad,
      sexo: perfil.sexo,
      direccion: perfil.direccion,
      fechaNacimiento: perfil.fechaNacimiento,
      nacionalidad: perfil.nacionalidad,
      telefono: perfil.telefono,
      user: perfil.user
    });
  }

  previousState() {
    window.history.back();
  }




  save() {

    const perfil = this.createFromForm();

    if (this.isSaving ) {

      this.subscribeToSaveResponse(this.perfilService.update(perfil));
    } else  {
      this.subscribeToSaveResponse(this.perfilService.create(perfil));
    }
  }

  private createFromForm(): Perfil {

    return {
      ...new Perfil(),
      id: this.editForm.get(['id']).value,
      tipoIdentificacion: this.editForm.get('tipoIdentificacion').value,
      identificacion: this.editForm.get('identificacion').value,
      edad: this.editForm.get('edad').value,
      sexo: this.editForm.get('sexo').value,
      direccion: this.editForm.get('direccion').value,
      fechaNacimiento:
        this.editForm.get('fechaNacimiento').value != null
          ? moment(this.editForm.get('fechaNacimiento').value, DATE_TIME_FORMAT)
          : undefined,
      nacionalidad: this.editForm.get('nacionalidad').value,
      telefono: this.editForm.get('telefono').value,
      user: this.usuario
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPerfil>>) {
    result.subscribe((res: HttpResponse<IPerfil>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }


  trackUserById(index: number, item: User) {
    return item.id;
  }
}
