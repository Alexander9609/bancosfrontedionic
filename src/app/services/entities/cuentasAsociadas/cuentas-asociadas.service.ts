import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from 'src/app/services/api/api.service';
import { ICuentasAsociadas } from '../../../shared/model/cuentas-asociadas.model';

type EntityResponseType = HttpResponse<ICuentasAsociadas>;
type EntityArrayResponseType = HttpResponse<ICuentasAsociadas[]>;

@Injectable({ providedIn: 'root' })
export class CuentasAsociadasService {
  public resourceUrl = ApiService.API_URL + '/cuentas-asociadas';

  constructor(protected http: HttpClient) {}

  create(cuentasAsociadas: ICuentasAsociadas): Observable<EntityResponseType> {
    return this.http.post<ICuentasAsociadas>(this.resourceUrl, cuentasAsociadas, { observe: 'response' });
  }

  update(cuentasAsociadas: ICuentasAsociadas): Observable<EntityResponseType> {
    return this.http.put<ICuentasAsociadas>(this.resourceUrl, cuentasAsociadas, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICuentasAsociadas>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  findCuentasByUserId(id: number): Observable<EntityResponseType> {
    return this.http.get<ICuentasAsociadas>(`${this.resourceUrl}?userId.equals=${id}`, { observe: 'response' });
  }

  

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
