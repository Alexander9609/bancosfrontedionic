import { TestBed } from '@angular/core/testing';

import { CuentasAsociadasService } from './cuentas-asociadas.service';

describe('CuentasAsociadasService', () => {
  let service: CuentasAsociadasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CuentasAsociadasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
