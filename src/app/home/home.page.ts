import { Component } from '@angular/core';
import { LoginService } from 'src/app/services/login/login.service';
import { NavController, ModalController, LoadingController, Platform } from '@ionic/angular';
import { PerfilService } from '../services/entities/perfil/perfil.service';
import { AccountService } from 'src/app/services/auth/account.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private accountService: AccountService, private perfilService: PerfilService, private loginService: LoginService, public navController: NavController,) { }

  ngOnInit() {
    this.getPerfilUser();
  }

  cuentasAsociadas(){
    this.navController.navigateBack('/cuentas-asociadas')
  }
  getPerfilUser() {
    this.accountService.identity().then((account) => {

      if (account == null) {
        this.navController.navigateBack('/login')
      } else {
        localStorage.setItem('dataAccount', JSON.stringify(account))

        this.perfilService.findByUser(account.id).subscribe(succes => {
          if (succes.body == "") {
            this.navController.navigateBack('tabs/perfil-creats')
          }

        }, error => {
          console.log("##########", error);

        })
      }


    });

  }

  logout() {
    this.loginService.logout();
    this.goBackToHomePage();
  }
  private goBackToHomePage(): void {
    this.navController.navigateBack('');
  }
}
